Rails.application.routes.draw do
  root 'line_of_credits#index'
  resources :transactions, except: [:index]
  resources :draws
  resources :payments
  resources :line_of_credits do
    resources :transactions, only: [:index]
  end
end
