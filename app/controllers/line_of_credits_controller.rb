class LineOfCreditsController < ApplicationController
  before_action :set_line_of_credit, only: [:show, :edit, :update, :destroy]
  before_action :set_transactions, only: [:show]
  
  def index
    @line_of_credits = LineOfCredit.all
  end

  def show
  end

  def new
    @line_of_credit = LineOfCredit.new
  end

  def edit
  end

  def create
    @line_of_credit = LineOfCredit.new(line_of_credit_params)

    respond_to do |format|
      if @line_of_credit.save
        format.html { redirect_to @line_of_credit, notice: 'Line of credit was successfully created.' }
        format.json { render :show, status: :created, location: @line_of_credit }
      else
        format.html { render :new }
        format.json { render json: @line_of_credit.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @line_of_credit.update(line_of_credit_params)
        format.html { redirect_to @line_of_credit, notice: 'Line of credit was successfully updated.' }
        format.json { render :show, status: :ok, location: @line_of_credit }
      else
        format.html { render :edit }
        format.json { render json: @line_of_credit.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @line_of_credit.destroy
    respond_to do |format|
      format.html { redirect_to line_of_credits_url, notice: 'Line of credit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_line_of_credit
      @line_of_credit = LineOfCredit.find(params[:id])
    end

    def set_transactions
      @thirty_day_interest = @line_of_credit.thirty_day_interest
      @principal_balance = @line_of_credit.principal_balance
      @total_payoff = @thirty_day_interest + @principal_balance
    end


    def line_of_credit_params
      params.require(:line_of_credit).permit(:apr, :credit_limit, :principal)
    end
end
