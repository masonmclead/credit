class TransactionsController < ApplicationController
  before_action :set_line_of_credit, only: [:index]
  before_action :set_transaction, only: [:show, :destroy]

  def index
    @transactions = @line_of_credit.transactions
    respond_to do |format|
      format.js
    end
  end

  def create 
    @line_of_credit = LineOfCredit.find(transaction_params[:line_of_credit_id])    
    params["transaction"]["applied_at"] = @line_of_credit.created_at + params["transaction"]["applied_at"].to_i.days
    
    @transaction = Transaction.new(transaction_params)
    
    respond_to do |format|
      if @transaction.save
        format.js { render :js => "window.location.href='"+line_of_credit_path(@line_of_credit.id)+"'" }
      else
        logger.error @transaction.errors.messages
        format.js   { render partial: 'transactions/transaction_form_errors', status: :unprocessable_entity  }
      end
    end
  end

  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to @transaction.line_of_credit, notice: 'Transaction was successfully destroyed.' }
      format.js { head :no_content, status: :success }
    end
  end

  private
    
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    def set_line_of_credit
      @line_of_credit = LineOfCredit.find(params[:line_of_credit_id])
    end

    def transaction_params
      params.require(:transaction).permit(:line_of_credit_id, :amount, :type, :applied_at)
    end
end
