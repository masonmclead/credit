ready = ->

  loadTransactionsTable =  (getUrl) ->
    $('#transactions_table').html("<span>Loading Transactions</span>")
    $.get(transactionsGetUrl)

  lineOfCreditId = $("#line_of_credit_id").data('id')
  transactionsGetUrl = "/line_of_credits/"+lineOfCreditId+"/transactions.js"

  loadTransactionsTable(transactionsGetUrl)

  $("#transaction_form form").on( "ajax:error", (e, xhr, status, error) ->
    $("#transaction_form #error_holder").html(xhr.responseText))

$(document).on('page:load ready', ready)
