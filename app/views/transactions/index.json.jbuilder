json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :line_of_credit_id, :amount, :type
  json.url transaction_url(transaction, format: :json)
end
