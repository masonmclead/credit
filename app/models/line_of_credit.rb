class LineOfCredit < ActiveRecord::Base

  validates :credit_limit, :apr, presence: true, numericality: {greater_than_or_equal_to: 0.0}

  DAYS_IN_YEAR = 365

  has_many :transactions, -> { order "transactions.applied_at ASC" }

  TRANSACTION_TYPES = %w{draws payments interest_accruals}

  TRANSACTION_TYPES.each do |transaction_type|
    has_many transaction_type.to_sym, -> { order "transactions.applied_at ASC" }

    define_method(:"total_#{transaction_type}") do | at_time |
      at_time ||= end_date
      self.send(transaction_type)
        .where("applied_at <= :point_in_time", {point_in_time: at_time})
        .sum(:amount)
    end
  end

  def principal_balance at_time=end_date
    transactions.where("applied_at <= :point_in_time", {point_in_time: at_time}).reduce(0) {|sum, transaction| 
      sum + transaction.amount * transaction.class.sign
    }
  end

  def current_interest at_time=end_date
    return 0.0 unless draws.first
    start_time = draws.first.applied_at
    days_old = TimeDifference.between(start_time, at_time).in_days.to_i
    (0..days_old).to_a.reduce(0) do |sum, days_ago|
      sum + ((principal_balance(at_time - days_ago.days) * apr / 100 / DAYS_IN_YEAR))
    end
  end

  def thirty_day_interest
    return 0.0 unless draws.first
    start_time = draws.first.applied_at
    days_old = TimeDifference.between(start_time, end_date).in_days.to_i
    ((days_old - 29)..(days_old)).to_a.reduce(0) do |sum, days_ago|
      sum + (principal_balance(end_date - days_ago.days) * apr / 100 / DAYS_IN_YEAR)
    end
  end

  def end_date
    created_at + 30.days
  end
end
