class Transaction < ActiveRecord::Base
  belongs_to :line_of_credit

  validates :applied_at, presence: true
  validates :amount, numericality: {greater_than: 0}
  validate :amount_can_not_exceed_credit_limit

  def self.sign
    raise "Must define in subtype"
  end

  def amount_can_not_exceed_credit_limit
    if (line_of_credit.principal_balance + amount * self.class.sign) > line_of_credit.credit_limit
      errors.add(:amount, 'cannot exceed the credit limit') 
    end
  end
end
