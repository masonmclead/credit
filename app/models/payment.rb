class Payment < Transaction

  validate :amount_can_not_take_the_balance_negative

  def self.sign
    -1
  end

  def amount_can_not_take_the_balance_negative
    if (line_of_credit.principal_balance + (amount * self.class.sign)) < 0
      errors.add(:amount, 'cannot exceed what is owed') 
    end
  end
end