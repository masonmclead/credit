class AddAppliedAtDateToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :applied_at, :datetime
  end
end
