class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :line_of_credit, index: true, foreign_key: true
      t.float :amount
      t.string :type

      t.timestamps null: false
    end
  end
end
