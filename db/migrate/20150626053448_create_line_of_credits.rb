class CreateLineOfCredits < ActiveRecord::Migration
  def change
    create_table :line_of_credits do |t|
      t.float :apr
      t.float :credit_limit
      t.float :principal

      t.timestamps null: false
    end
  end
end
