require 'rails_helper'

RSpec.describe LineOfCredit, type: :model do
  
  describe 'factory' do
    
    let(:line_of_credit) { FactoryGirl.build(:line_of_credit) }
    
    it "builds a valid line_of_credit" do
      expect(line_of_credit.valid?).to be_truthy
    end
  end

  describe 'validations' do
    it 'validates that APR is greater than or equal to 0' do
      line_of_credit = FactoryGirl.build(:line_of_credit, apr: -0.1)
      expect(line_of_credit.valid?).to be_falsey

      line_of_credit = FactoryGirl.build(:line_of_credit, apr: 0.1)
      expect(line_of_credit.valid?).to be_truthy
    end

    it 'validates that credit_limit is greater than or equal to 0' do
      line_of_credit = FactoryGirl.build(:line_of_credit, credit_limit: -1)
      expect(line_of_credit.valid?).to be_falsey

      line_of_credit = FactoryGirl.build(:line_of_credit, credit_limit: 1)
      expect(line_of_credit.valid?).to be_truthy
    end
  end

  describe 'associations' do
    it {should have_many :draws}
    it {should have_many :payments}
    it {should have_many :interest_accruals}
  end

  describe '#principal_balance' do
    let(:line_of_credit) { FactoryGirl.create(:line_of_credit) }
    context 'when there are no draws' do
      it 'returns a total of zero' do
      end
    end
    context 'when there are draws' do
      let!(:amount_per_draw) { 20 }
      let!(:number_of_draws) { 3 }
      let(:total_of_draws) { amount_per_draw * number_of_draws }

      before do
        FactoryGirl.create_list(:draw, number_of_draws, line_of_credit: line_of_credit, amount: amount_per_draw)
      end
      it 'returns the totals of the draw amounts' do
        expect(line_of_credit.principal_balance).to eq (total_of_draws)
      end

      context 'and there are payments' do
        let!(:amount_per_payment) { 5 }
        let!(:number_of_payments) { 2 }
        let(:total_of_payments) { amount_per_payment * number_of_payments }
        before do
          FactoryGirl.create_list(:payment, number_of_payments, line_of_credit: line_of_credit, amount: amount_per_payment)
        end
        it 'returns the totals of the draw amounts minus the amount of payments' do
          expect(line_of_credit.principal_balance).to eq (total_of_draws - total_of_payments)
        end

        context 'and there are interest_accruals' do
          let!(:amount_per_accrual) { 0.12 }
          let!(:number_of_accruals) { 2 }
          let(:total_of_accruals) { amount_per_accrual * number_of_accruals }
          before do
            FactoryGirl.create_list(:interest_accrual, number_of_accruals, line_of_credit: line_of_credit, amount: amount_per_accrual)
          end
          it 'returns the totals of the draw amounts plus the interest_accruals minus the payments' do
            expect(line_of_credit.principal_balance.round(2)).to eq (total_of_draws + total_of_accruals - total_of_payments)
          end
        end
      end
    end
  end

  describe '#current_interest' do
    let(:line_of_credit) { FactoryGirl.create(:line_of_credit) }
    context 'when the draw is not yet 30 days old' do
      let!(:draw) { FactoryGirl.create(:draw, line_of_credit: line_of_credit, applied_at: line_of_credit.created_at + (15+1).days ) }
      it 'shows the current interest at the present day' do
        expect(line_of_credit.current_interest.round(2)).to eq (line_of_credit.apr / 100 * draw.amount / LineOfCredit::DAYS_IN_YEAR * 15).round(2)
      end
    end
  end

  describe '#thirty_day_interest' do
    let(:line_of_credit) { FactoryGirl.create(:line_of_credit) }
    context 'when there is one draw at the beginning' do
      let!(:draw) { FactoryGirl.create(:draw, line_of_credit: line_of_credit, applied_at: line_of_credit.created_at + (30).days) }
      it 'returns apr times the draw amount' do
        expect(line_of_credit.thirty_day_interest.round(2)).to eq (line_of_credit.apr / 100 * draw.amount / LineOfCredit::DAYS_IN_YEAR * 30).round(2)
      end
      context 'when the draw is not yet 30 days old' do
        let!(:draw) { FactoryGirl.create(:draw, line_of_credit: line_of_credit, applied_at: line_of_credit.created_at + (15).days) }
        it 'shows the predicted interest at the end of 30 day period' do
          expect(line_of_credit.thirty_day_interest.round(2)).to eq (line_of_credit.apr / 100 * draw.amount / LineOfCredit::DAYS_IN_YEAR * 30).round(2)
        end
      end
    end

    context 'when there are many draws' do
      let(:draws) { FactoryGirl.build_list(:draw, 3, line_of_credit: line_of_credit) }
      it 'returns apr times the principal_balance per day' do
        draws.each_with_index do |draw, index|
          draw.update(applied_at: line_of_credit.created_at - (30/(index+1)-1).days)
        end
        expected_interest_amount = 
          (line_of_credit.apr/ 100 * draws[0].amount / LineOfCredit::DAYS_IN_YEAR * 30) +
          (line_of_credit.apr/ 100 * draws[1].amount / LineOfCredit::DAYS_IN_YEAR * 15) +
          (line_of_credit.apr/ 100 * draws[2].amount / LineOfCredit::DAYS_IN_YEAR * 10)
        expect(line_of_credit.thirty_day_interest.round(2)).to eq expected_interest_amount.round(2)
      end
    end
  end

end
