require 'rails_helper'

RSpec.describe Draw, type: :model do

  describe 'factory' do
    let(:draw) { FactoryGirl.build(:draw) }
    
    it "builds a valid draw" do
      expect(draw.valid?).to be_truthy
    end
  end

  describe '.sign' do
    it 'returns positive 1' do
      expect(Draw.sign).to eq 1
    end
  end
end