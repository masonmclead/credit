require 'rails_helper'

RSpec.describe Payment, type: :model do

  describe '.sign' do
    it 'returns negative 1' do
      expect(Payment.sign).to eq -1
    end
  end

  describe 'factory' do
    let!(:line_of_credit) {FactoryGirl.create(:line_of_credit, credit_limit: 1000)}
    let(:payment) { FactoryGirl.build(:payment, amount: 100, line_of_credit: line_of_credit) }
    
    before do 
      FactoryGirl.create(:draw, amount: 100, line_of_credit: line_of_credit, applied_at: line_of_credit.created_at + 1.day)
    end
    it "builds a valid payment" do
      
      expect(payment.valid?).to be_truthy
    end

    describe 'validations' do
      it 'does not allow a payment over the principal balance' do
        payment.amount = line_of_credit.principal_balance + 1
        expect(payment.valid?).to be_falsey
        expect(payment.errors.messages.keys).to include :amount

      end
    end
  end

  


end