require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe 'validation' do
    let!(:line_of_credit) {FactoryGirl.create(:line_of_credit)}
    let(:draw) { FactoryGirl.build(:draw, line_of_credit: line_of_credit) }

    it 'does not allow a draw larger than the credit limit' do
      draw.amount = line_of_credit.credit_limit + 1
      expect(draw.valid?).to be_falsey
      expect(draw.errors.messages.keys).to include :amount
    end
  end
end
