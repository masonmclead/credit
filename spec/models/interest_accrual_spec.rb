require 'rails_helper'

RSpec.describe InterestAccrual, type: :model do

  describe 'factory' do
    let(:interest_accrual) { FactoryGirl.build(:interest_accrual) }
    
    it "builds a valid interest_accrual" do
      expect(interest_accrual.valid?).to be_truthy
    end
  end

  describe '.sign' do
    it 'returns positive 1' do
      expect(InterestAccrual.sign).to eq 1
    end
  end
end