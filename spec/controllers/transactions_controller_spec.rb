require 'rails_helper'

RSpec.describe TransactionsController, type: :controller do

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:draw, applied_at: 1 )
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:draw, amount: -344.0)
  }

  describe "GET #index" do
    context 'when format is JS' do
      let!(:line_of_credit) { FactoryGirl.create(:line_of_credit) }
      let(:transaction) { FactoryGirl.create(:draw, line_of_credit: line_of_credit) }
      
      subject { xhr :get, :index, {line_of_credit_id: line_of_credit.id, format: 'js'} }

      before do 
        subject
      end
      
      it 'assigns @line_of_credit' do
        expect(assigns(:line_of_credit)).to eq(line_of_credit)
      end
    
      it "assigns all transactions for @line_of_credit as @transactions" do
        expect(assigns(:transactions)).to eq([transaction])
      end
      it 'renders the transactions index js view' do
        expect(subject).to render_template("transactions/index")
      end

    end
  end

  describe "POST #create" do
    context "with valid params" do
      before do
        valid_attributes[:line_of_credit_id] = FactoryGirl.create(:line_of_credit).id
      end
      it "creates a new Transaction" do
        expect {
          post :create, {:transaction => valid_attributes, format: :js}
        }.to change(Transaction, :count).by(1)
      end

      it "assigns a newly created transaction as @transaction" do
        post :create, {:transaction => valid_attributes, format: :js}
        expect(assigns(:transaction)).to be_a(Transaction)
        expect(assigns(:transaction)).to be_persisted
      end
    end

    context "with invalid params" do
      before do
        invalid_attributes[:line_of_credit_id] = FactoryGirl.create(:line_of_credit).id
      end
      it "assigns a newly created but unsaved transaction as @transaction" do
        post :create, {:transaction => invalid_attributes, format: :js}
        expect(assigns(:transaction)).to be_a_new(Transaction)
      end

      it "re-renders the 'new' template" do
        post :create, {:transaction => invalid_attributes, format: :js}
        expect(response).to render_template("transactions/_transaction_form_errors")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested transaction" do
      transaction = FactoryGirl.create(:draw)
      expect {
        delete :destroy, {:id => transaction.to_param}
      }.to change(Transaction, :count).by(-1)
    end

    it "redirects to the transactions list" do
      transaction = FactoryGirl.create(:draw)
      delete :destroy, {:id => transaction.to_param}
      expect(response).to redirect_to(transaction.line_of_credit)
    end
  end

end
