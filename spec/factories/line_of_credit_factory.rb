FactoryGirl.define do
  factory :line_of_credit do
    apr { rand(0.0..40.0) }
    credit_limit { 1000 }
    principal { 0 }
  end
end
