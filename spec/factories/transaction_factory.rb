FactoryGirl.define do
  factory :transaction do
    line_of_credit
    amount { rand(0.0..300.0) }
    applied_at { Time.now }
  
    factory :draw, class: 'Draw' do
      type "Draw"
    end  
    factory :payment, class: 'Payment' do
      type "Payment"
    end
    factory :interest_accrual, class: "InterestAccrual" do
      type "InterestAccrual"
    end
  end
end
